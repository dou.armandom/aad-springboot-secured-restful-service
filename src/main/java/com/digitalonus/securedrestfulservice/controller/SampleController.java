/**
 *
 * @author Armando Montoya
 * @email armando.montoya@digitalonus.com
 * @Date Apr 24, 2020
 *
 */

package com.digitalonus.securedrestfulservice.controller;

import com.digitalonus.securedrestfulservice.dto.Contact;
import com.digitalonus.securedrestfulservice.services.SampleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class SampleController {
    private final SampleService sampleService;

    @GetMapping("/samples")
    @PreAuthorize("hasRole('ROLE_USER')")   // This is validating against Active Directory's User role granted to the
                                            // current user.
    @ResponseStatus(HttpStatus.OK)
    public Contact getContactDetails() {
        return sampleService.printDetails();
    }
}
